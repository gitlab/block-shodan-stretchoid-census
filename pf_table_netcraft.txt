#- -------------netcraft.com
#- DigitalOcean block list
#- This list if dedicated to netcraft ONLY. The problem is that
#- netcraft is swithing between hosts to fast and it is not really
#- possible to map all NETS of the didigtal ocean and linode,
#- so this list blocks whole subnets.

#- ATTENTION! instead of collecting both linode and netcraft nets
#- in one file, from now on you have to change the rule to
#- pf_table_linode.txt and pf_table_digitalocean.txt
#- This file contains only some fixed hosts which remains the same 
#- for a long period of time.


#134.209.25.185/32 # nantucket.netcraft.com.
#134.209.178.29/32 # skimmers.netcraft.com.
#134.209.179.159/32 # mardegrau.netcraft.com.
#134.209.182.34/32 # alboran.netcraft.com.
#134.209.184.234/32 # beaufort.netcraft.com.

#45.55.76.
45.55.76.116/32 # adirondack.netcraft.com.
52.30.171.229/32 # mackerel.toolbar.netcraft.com
52.16.137.212/32 # minnow.toolbar.netcraft.com
165.227.27.40/32 # 3(NXDOMAIN)
167.99.102.139 # 3(NXDOMAIN)

#-- Some usefull information
#-netcraft reverse signatures: 
#- 0f18e2bd-541e-4efd-8417-946a0cf2ac77.23b1cb84-939b-42bc-8f54-90bcda5fba00
#- prod-nyc3.qencode-master-96b70e78a4f411eaae4d76050b26adc7
#- eifft-master.agora.dev.deon.digital <--- ????
#- claireaustin.production-1526246560142-s-2vcpu-4gb-lon1-01
#- *.netcraft.com
#- wounded-05.gz-s-2vcpu-4gb-sfo2-01.
#- kraken-2-0-443-20000-nyc1-0.0.0.0-0.
#- internet-scanner.eset.com. <---???

#?
46.101.40.190/32 # 3(NXDOMAIN) ?
178.128.194.144/32 # 3(NXDOMAIN) ?
178.128.194.144/32
167.99.109.221/32

54.78.39.0/24 # amazon
34.245.70.0/24 # amazon
3.249.232.0/24 # amazon

