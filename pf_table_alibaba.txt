# 8.128.0.0 - 8.159.255.255
8.128.0.0/11 # ALICLOUD Aliyun Computing Co.LTD

8.208.0.0/12 # Alibaba.com Singapore E-Commerce Private Limited
8.208.0.0/16 # Aliyun Computing Co.LTD Singapore Alibabacom

14.1.112.0/22 # Alibaba.com Singapore E-Commerce Private Limited
 
# Aliyun Computing Co., LTD ALISOFT Alibaba (US) Technology Co., Ltd
39.96.0.0/13
39.104.0.0/14
39.108.0.0/16


# 47.56.0.0 - 47.57.255.255
47.56.0.0/15 # ALICLOUD-HK #notice: running bot which gathers contact info and other personal data

# 47.74.0.0 - 47.87.255.255
47.74.0.0/15
47.76.0.0/14
47.80.0.0/13

47.88.0.0/14 # 47.88.0.0 - 47.91.255.255 AL-3

47.96.0.0/15 # Alibaba (US) Technology Co., Ltd. Hangzhou Alibaba Advertising Co.,Ltd.

47.100.0.0/14 # Aliyun Computing Co., LTD ALISOFT Alibaba (US) Technology Co., Ltd.

47.235.0.0/16
47.236.0.0/14
47.240.0.0/14 
47.244.0.0/15 
47.246.0.0/16 

47.250.0.0/15 #  Alibaba.com LLC (AL-3)
47.252.0.0/15 #  Alibaba.com LLC (AL-3)
47.254.0.0/16 #  Alibaba.com LLC (AL-3)



47.240.0.0/14 # /47.235.0.0 - 47.246.255.255
47.235.0.0/16 # |
47.246.0.0/16 # | Alibaba.com LLC (AL-3) ALICLOUD-SG
47.236.0.0/14 # |
47.244.0.0/15 # /

147.139.0.0/16 # ALICLOUD-ID Alibaba.com LLC (AL-3)

149.129.128.0/18 # ALICLOUD-IN Alibaba.com Singapore E-Commerce Private Limited

