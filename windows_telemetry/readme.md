# Blocking Microsoft Telemetry Cr@p (Radical Method)

How to radically eradicate Microsoft telemetry which is spying on you?

There are:
- akamai_cidr.txt
- amazon_cidr.txt
- micosoft_servers_cidr.txt
- microsoft_servers_hostnames.txt
- stackpath_cidr.txt
- verizone_cidr.txt

files which contains IP/CIDR and hostnames, which were detected while running Windows 10.  

You need:
- A firewall like: pfSense, Opnsense or other.
- Set a fixed IP address for your PC which is running Windows i.e DHCP or static address.

The firewall rules should block any outgoing (and optionally incoming) traffic to the IP ranges from the list. The strategy to block by hostname or set local resolver to resolve the domains to 127.0.0.1 is not efficient and completely useless.

## Example setup
OPNsense (firewall PF)  
```
# For the IPV4 NAT 
block drop in log quick on <WAN_interface> inet from <fixed_ip_WinPC> to <list_from_repo>

# for example
block drop in log quick on bge0 inet from 192.168.2.10 to <microsoft_ip>
```

The pros of this approach are: Windows Telemetry will be completely cut.  
The cons of this approach are: The internet connection of the windows machine will be half cut from the internet.

But there is a solution.  
You need to install some virtual machine monitor like VirtualBox or VMWare (both are proprietary cr@p). Creating guest VM and as a guest VM running Linux or FreeBSD or OpenBSD or any other OS which does not track you. A guest VM network mode is set to `Bridged` mode. In the guest VM installing proxy server. Then in your browser on host OS (Windows) for any software which requires full access, set the proxy address of your VM.

i.e
```
 HOST (Windows) ---fixed IP---> FIREWALL ----> filtered
      (Firefox) ---\
      (other soft)-| proxy
                   V
 GUEST (FreeBSD) ---proxied---> FIREWALL -----> Not fltered
```
